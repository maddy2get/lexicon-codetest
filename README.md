# LexiconCodetest

## Prerequisite

1. Latest version of `node.js`.
2. Set `LEXICON_API_KEY` environment variable using your api key.
3. Run `npm install` in the project directory.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io). Code coverage is generated and will be stored in the `coverage/` directory.

## Assumptions

1. Both movie providers has same movies
2. Both movie providers has movies with same `ID` with prefix `cw` or `fw`
3. Movies are considered unique by their `title` as there is not unique identifier in both movie providers

## Archtectural elements and decisions

1. Used `Angular` to quickly implement required functionality as it provides built-in routing and http services.
2. Used Angular's proxy config to come over CORS issue without needing a backend server for development envirnment
3. Have not added css because of time restraint 
