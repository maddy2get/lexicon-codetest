export interface Movie {
  ID: string,
  Poster: string,
  Title: string,
  Type: string,
  CinemaworldPrice?: number;
  FilmworldPrice?: number;
  Price?: number;
}

export interface MovieProvider {
  Provider: string;
  Movies: Movie[]
}
