import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MovieApiService } from './movie-api.service';
import { environment } from 'src/environments/environment';
import { MOVIE, MOVIE_PROVIDER } from './movie.mocks';

describe('MovieApiService', () => {
  let service: MovieApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MovieApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call both cinemaworld and filmworld api endpoints for movies', () => {
    const movies = [MOVIE_PROVIDER, MOVIE_PROVIDER];
    service.getMovies().subscribe(results => {
      expect(results.length).toEqual(2);
      expect(results).toEqual(movies);
    });

    const cinemaworldReq = httpMock.expectOne(`${environment.apiUrl}/api/cinemaworld/movies`);
    const filmWorldReq = httpMock.expectOne(`${environment.apiUrl}/api/filmworld/movies`);
    expect(cinemaworldReq.request.method).toBe("GET");
    expect(filmWorldReq.request.method).toBe("GET");
    cinemaworldReq.flush(MOVIE_PROVIDER);
    filmWorldReq.flush(MOVIE_PROVIDER);
  });

  it('should call both cinemaworld and filmworld api endpoints for movie', () => {
    service.getMovie('id').subscribe(results => {
      expect(results.length).toEqual(2);
      expect(results).toEqual([MOVIE, MOVIE]);
    });

    const cinemaworldReq = httpMock.expectOne(`${environment.apiUrl}/api/cinemaworld/movie/cwid`);
    const filmWorldReq = httpMock.expectOne(`${environment.apiUrl}/api/filmworld/movie/fwid`);
    expect(cinemaworldReq.request.method).toBe("GET");
    expect(filmWorldReq.request.method).toBe("GET");
    cinemaworldReq.flush(MOVIE);
    filmWorldReq.flush(MOVIE);
  });
});
