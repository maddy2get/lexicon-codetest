import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { MovieResolverService } from './movie-resolver.service';
import { MOVIE } from './movie.mocks';
import { MovieService } from './movie.service';

describe('MovieResolverGuardService', () => {
  let service: MovieResolverService;
  let router: Router;
  let route: ActivatedRouteSnapshot;
  let movieService: MovieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        MovieService,
        {
          provide: ActivatedRouteSnapshot,
          useValue: {
            params: {
              id: 'id'
            }
          }
        }
      ]
    });
    service = TestBed.inject(MovieResolverService);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRouteSnapshot);
    movieService = TestBed.inject(MovieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should resolve and get movie', () => {
    spyOn(movieService, 'getMovie').and.returnValue(of(MOVIE));
    service.resolve(route).subscribe((resolved) => {
      expect(resolved).toEqual(MOVIE);
    });
  });

  it('should resolve and return null on api error', () => {
    spyOn(movieService, 'getMovie').and.returnValue(throwError(
      new HttpErrorResponse({
        status: 500
      })
    ));
    service.resolve(route).subscribe((resolved) => {
      expect(resolved).toEqual(null);
    });
  });

  it('should not resolve on 404 api error and navigate to classic-movies route', () => {
    spyOn(movieService, 'getMovie').and.returnValue(throwError(
      new HttpErrorResponse({
        status: 404
      })
    ));
    spyOn(router, 'navigate');
    service.resolve(route).subscribe((resolved) => {
      expect(resolved).toEqual(null);
    });
    expect(router.navigate).toHaveBeenCalledWith(['classic-movies']);
  });
});
