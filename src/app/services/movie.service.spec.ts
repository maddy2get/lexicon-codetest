import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { MovieApiService } from './movie-api.service';
import { FILTERED_MOVIE, MOVIE, MOVIE_ID, MOVIE_PROVIDER } from './movie.mocks';

import { MovieService } from './movie.service';

describe('MovieService', () => {
  let service: MovieService;
  let movieApiService: MovieApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(MovieService);
    movieApiService = TestBed.inject(MovieApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('it should get consolidated movies', () => {
    spyOn(movieApiService, 'getMovies').and.returnValue(of([MOVIE_PROVIDER, MOVIE_PROVIDER]));
    service.getMovies().subscribe((result) => {
      expect(result).toEqual([FILTERED_MOVIE]);
    });
    expect(movieApiService.getMovies).toHaveBeenCalled();
  });

  it('it should get movie', () => {
    spyOn(movieApiService, 'getMovie').and.returnValue(of([
      {...MOVIE, Price: 22},
      {...MOVIE, Price: 24}
    ]));
    service.getMovie(MOVIE.ID).subscribe((result) => {
      expect(result).toEqual({
        ...MOVIE,
        CinemaworldPrice: 22,
        FilmworldPrice: 24
      });
    });
    expect(movieApiService.getMovie).toHaveBeenCalled();
  });
});
