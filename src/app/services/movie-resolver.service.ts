import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Movie } from './movie.models';
import { MovieService } from './movie.service';

@Injectable({
  providedIn: 'root'
})
export class MovieResolverService implements Resolve<Movie|null> {

  constructor(private readonly router: Router,
    private readonly movieService: MovieService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Movie|null> {
    return this.movieService.getMovie(route.params['id'])
      .pipe(
        map(data => data),
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.router.navigate(['classic-movies']);
          }
          return of(null);
        })
      )
  }
}
