import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie, MovieProvider } from './movie.models';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'x-api-key': environment.API_KEY as string
  })
}

@Injectable({
  providedIn: 'root'
})
export class MovieApiService {

  constructor(private readonly http: HttpClient) { }

  getMovies(): Observable<[MovieProvider, MovieProvider]> {
    const CINEMAWORLD_MOVIES: string = `${environment.apiUrl}/api/cinemaworld/movies`;
    const FILMWORLD_MOVIES: string = `${environment.apiUrl}/api/filmworld/movies`;

    let cinemaworldMovies = this.http.get<MovieProvider>(CINEMAWORLD_MOVIES, httpOptions);
    let filmworldMovies = this.http.get<MovieProvider>(FILMWORLD_MOVIES, httpOptions);

    return forkJoin([cinemaworldMovies, filmworldMovies]);
  }

  getMovie(id: string): Observable<[Movie, Movie]> {
    const CINEMAWORLD_MOVIE: string = `${environment.apiUrl}/api/cinemaworld/movie/cw${id}`;
    const FILMWORLD_MOVIE: string = `${environment.apiUrl}/api/filmworld/movie/fw${id}`;

    let cinemaworldMovie = this.http.get<Movie>(CINEMAWORLD_MOVIE, httpOptions);
    let filmworldMovie = this.http.get<Movie>(FILMWORLD_MOVIE, httpOptions);

    return forkJoin([cinemaworldMovie, filmworldMovie]);
  }
}
