import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClassicMoviesModule } from '../classic-movies/classic-movies.module';
import { MovieApiService } from './movie-api.service';
import { Movie, MovieProvider } from './movie.models';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  constructor(private readonly movieApiService: MovieApiService) { }

  getMovies(): Observable<Movie[]> {
    return this.movieApiService.getMovies().pipe(
      map(([cinemaWorldResults, filmWorldResults]: MovieProvider[]) => {
        let movies: Movie[] = [...cinemaWorldResults.Movies, ...filmWorldResults.Movies];
        return [...new Map(movies.map(item => [item['Title'], {...item, ID: item.ID.substr(2)}])).values()];
      })
    )
  }

  getMovie(id: string): Observable<Movie> {
    return this.movieApiService.getMovie(id).pipe(
      map(([cinemaWorldResult, filmWorldResult]: Movie[]) => {
        const movie: Movie = {
          ID: id,
          Title: cinemaWorldResult.Title,
          Type: cinemaWorldResult.Type,
          Poster: cinemaWorldResult.Poster,
          CinemaworldPrice: cinemaWorldResult.Price,
          FilmworldPrice: filmWorldResult.Price
        }
        return movie;
      })
    )
  }
}
