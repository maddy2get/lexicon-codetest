import { Movie, MovieProvider } from './movie.models';

export const MOVIE_ID: string = '12345678';

export const MOVIE: Movie = {
  ID: 'fw12345678',
  Title: 'Movie Title',
  Poster: '',
  Type: 'movie'
}

export const FILTERED_MOVIE = {...MOVIE, ID: MOVIE_ID};

export const MOVIE_PROVIDER: MovieProvider = {
  Movies: [MOVIE],
  Provider: 'Provider'
}
