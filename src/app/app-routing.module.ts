import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'classic-movies',
    loadChildren: () => import('./classic-movies/classic-movies.module').then(m => m.ClassicMoviesModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'classic-movies'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
