import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassicMoviesComponent } from './classic-movies.component';
import { ClassicMoviesRoutingModule } from './classic-movies-routing.module';
import { MovieComponent } from './movie/movie.component';
import { MovieResolverService } from '../services/movie-resolver.service';

@NgModule({
  declarations: [ClassicMoviesComponent, MovieComponent],
  imports: [
    CommonModule,
    ClassicMoviesRoutingModule
  ]
})
export class ClassicMoviesModule { }
