import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieResolverService } from '../services/movie-resolver.service';
import { ClassicMoviesComponent } from './classic-movies.component';
import { MovieComponent } from './movie/movie.component';

export const routes: Routes = [
  {
    path: '',
    component: ClassicMoviesComponent
  },
  {
    path: ':id',
    component: MovieComponent,
    resolve: {
      movie: MovieResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [MovieResolverService]
})
export class ClassicMoviesRoutingModule { }
