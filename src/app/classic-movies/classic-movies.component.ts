import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Movie } from '../services/movie.models';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-classic-movies',
  templateUrl: './classic-movies.component.html',
  styleUrls: ['./classic-movies.component.scss']
})
export class ClassicMoviesComponent implements OnInit {
  apiError = false;
  movies: Movie[] = [];

  constructor(private readonly movieService: MovieService) { }

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies() {
    this.movieService.getMovies()
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.apiError = true;
          return EMPTY;
        })
      )
      .subscribe((results: Movie[]) => {
        this.movies = results;
        this.apiError = false;
      });
  }

}
