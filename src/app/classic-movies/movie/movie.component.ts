import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from 'src/app/services/movie.models';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  movie!: Movie;
  constructor(private readonly route: ActivatedRoute,
    private readonly router: Router) { }

  ngOnInit(): void {
    this.movie = this.route.snapshot.data.movie;
  }

  /* istanbul ignore next */
  retry() {
    window.location.reload();
  }

  goToMovies() {
    this.router.navigate(['classic-movies']);
  }
}
