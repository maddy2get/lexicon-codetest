import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { MovieComponent } from './movie.component';

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MovieComponent ],
      imports: [
        RouterTestingModule.withRoutes([])
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to classic-movies on back button click', () => {
    spyOn(router, 'navigate');
    const compiled = fixture.nativeElement;
    compiled.querySelector('button.go-back').click();
    expect(router.navigate).toHaveBeenCalledWith(['classic-movies']);
  });

  it('should reload the page on retry', () => {
    spyOn(component, 'retry').and.callFake(() => {});
    const compiled = fixture.nativeElement;
    compiled.querySelector('button.retry').click();
    expect(component.retry).toHaveBeenCalled();
  });
});
