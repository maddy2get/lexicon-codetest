import { HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { FILTERED_MOVIE } from '../services/movie.mocks';
import { MovieService } from '../services/movie.service';

import { ClassicMoviesComponent } from './classic-movies.component';

describe('ClassicMoviesComponent', () => {
  let component: ClassicMoviesComponent;
  let fixture: ComponentFixture<ClassicMoviesComponent>;
  let movieService: MovieService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassicMoviesComponent ],
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        MovieService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassicMoviesComponent);
    component = fixture.componentInstance;
    movieService = TestBed.inject(MovieService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Classic Movies At Home');
  });

  it('should getMovies on component init', () => {
    spyOn(movieService, 'getMovies').and.returnValue(of([FILTERED_MOVIE]));
    component.ngOnInit();
    expect(movieService.getMovies).toHaveBeenCalled();
    expect(component.apiError).toEqual(false);
    expect(component.movies).toEqual([FILTERED_MOVIE]);
  });

  it('should show retry button if getMovies fails to load', () => {
    spyOn(movieService, 'getMovies').and.returnValue(throwError(
      new HttpErrorResponse({
        status: 400
      })
    ));
    component.ngOnInit();
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('button').textContent).toContain('Retry');
    expect(movieService.getMovies).toHaveBeenCalled();
    expect(component.apiError).toEqual(true);
  });

  it('should show retry button if getMovies fails to load', () => {
    spyOn(movieService, 'getMovies').and.returnValue(throwError(
      new HttpErrorResponse({
        status: 400
      })
    ));
    component.ngOnInit();
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain('Unable to load movies.');
    expect(movieService.getMovies).toHaveBeenCalled();
  });

  it('should call get movies on retry button click', () => {
    spyOn(movieService, 'getMovies').and.returnValue(throwError(
      new HttpErrorResponse({
        status: 400
      })
    ));
    spyOn(component, 'getMovies').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('.retry-button');
    button.click();
    expect(component.getMovies).toHaveBeenCalledTimes(2);
  });
});
