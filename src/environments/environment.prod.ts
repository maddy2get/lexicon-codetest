export const environment = {
  production: true,
  API_KEY: process.env.LEXICON_API_KEY,
  apiUrl: 'https://challenge.lexicondigital.com.au'
};
