const config = {
  "/api": {
    "target": 'https://challenge.lexicondigital.com.au',
    "secure": true,
    "changeOrigin": true,
    "pathRewrite": {
      "^/api": ""
    }
  }
}

module.exports = config;
