const webpack = require('webpack');
const keyPrefix = 'LEXICON_';

const keys = Object.keys(process.env).filter((key) => key.startsWith(keyPrefix));
let env = {};
keys.forEach(key => env[key] = JSON.stringify(process.env[key]));

module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      'process.env': env
    })
  ]
}
